﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemPricer
{
    public class Webelements
    {
        public static IWebElement element;

        //Home Page*****************
        //Text: URL field
        public static IWebElement txtSwaggerUrl(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//input[@id='input_baseUrl']"));
            return element;
        }

        //Button: Explore
        public static IWebElement btnExplore(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[@id='explore']"));
            return element;
        }

        //Header: Item
        //Tab: GET /{sellerName}/pricelist/{sellerPriceListId}/items
        public static IWebElement tabGETsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItems']/div[1]/h3/span[2]/a"));
            return element;
        }

        //Text: Seller Name
        public static IWebElement txtSllerNameGETsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItems_content']/form/table[1]/tbody/tr[1]/td[2]/input"));
            return element;
        }

        //Text: SellerPriceID
        public static IWebElement txtSellerPriceIDGETsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItems_content']/form/table[1]/tbody/tr[2]/td[2]/input"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryGETsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItems_content']/form/div[3]/input"));
            return element;
        }

        //Field: Response Code
        public static IWebElement ResponseCodeGETsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItems_content']/div[2]/div[4]/pre"));
            return element;
        }

        //++++++++++++++++++++
        //Tab: POST /{sellerName}/pricelist/{sellerPriceListId}/items
        public static IWebElement tabPOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems']/div[1]/h3/span[2]/a"));
            return element;
        }

        //Text: SellerName
        public static IWebElement txtSellerNamePOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems_content']/form/table[1]/tbody/tr[1]/td[2]/input"));
            return element;
        }

        public static IWebElement txtPriceListIDPOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems_content']/form/table[1]/tbody/tr[2]/td[2]/input"));
            return element;
        }

        public static IWebElement txtsellerItemIdsPOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems_content']/form/table[1]/tbody/tr[3]/td[2]/textarea"));
            return element;
        }


        public static IWebElement btnTryPOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems_content']/form/div[3]/input"));
            return element;
        }

        
                public static IWebElement ResponseCodePOSTsellerNamepricelistsellerPriceListIditems(IWebDriver driver)
            {
                element = driver.FindElement(By.XPath("//*[@id='Item_Item_PostItems_content']/div[2]/div[4]/pre"));
                return element;
            }

        //++++++++++++++++
        //Tab: GET /{sellerName}/pricelist/{sellerPriceListId}/items/{forThisDate}
        public static IWebElement tabGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/{sellerName}/pricelist/{sellerPriceListId}/items/{forThisDate}']"));
            return element;
        }

        //Text: SellerName
        public static IWebElement txtSellerNameGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByDate_content']/form/table[1]/tbody/tr[1]/td[2]/input"));
            return element;
        }

        //Text: sellerPriceListId
        public static IWebElement txtsellerPriceListIdGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByDate_content']/form/table[1]/tbody/tr[2]/td[2]/input"));
            return element;
        }

        //Text: forThisDate
        public static IWebElement txtforThisDateGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByDate_content']/form/table[1]/tbody/tr[3]/td[2]/input"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByDate_content']/form/div[3]/input"));
            return element;
        }

        //Field: ResponseCode
        public static IWebElement ResponseCodeGETsellerNamepricelistsellerPriceListIditemsforThisDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByDate_content']/div[2]/div[4]/pre"));
            return element;
        }

        //++++++++++++++++++
        //Tab: GET /{sellerName}/pricelist/{sellerPriceListId}/items/{fromDate}/{toDate}
        public static IWebElement tabGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange']/div[1]/h3/span[2]/a"));
            return element;
        }

        //Text: sellerName
        public static IWebElement txtSellerNameGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/form/table[1]/tbody/tr[1]/td[2]/input"));
            return element;
        }

        //Text: sellerPriceListId
        public static IWebElement txtsellerPriceListIdGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/form/table[1]/tbody/tr[2]/td[2]/input"));
            return element;
        }

        //Text: fromDate
        public static IWebElement txtfromDateGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/form/table[1]/tbody/tr[3]/td[2]/input"));
            return element;
        }
        //Text: toDate
        public static IWebElement txttoDateGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/form/table[1]/tbody/tr[4]/td[2]/input"));
            return element;
        }
        //Button: Try
        public static IWebElement btnTryGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/form/div[3]/input"));
            return element;
        }

        //Field: ResponseCode
        public static IWebElement ResponseCodeGETsellerNamepricelistsellerPriceListIditemsfromDatetoDate(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Item_Item_GetItemsByRange_content']/div[2]/div[4]/pre"));
            return element;
        }

        
    }
}
