﻿using Dell.Commerce.Services.LoggingProviders;

namespace Dell.ItemPricer.WebAPI.Management_Core.Models
{
    /// <summary>
    /// InstrumentationManager
    /// </summary>
    public class InstrumentationManager
    {

        private static InstrumentationType _activeInstrumentationType = InstrumentationType.IncludeSLA;



        // GET api/instrumentation/5
        /// <summary>
        /// Get
        /// </summary>
        /// <returns></returns>
        public static InstrumentationType Get()
        {
            return _activeInstrumentationType;
        }

        /// <summary>
        /// GetAsInt
        /// </summary>
        /// <returns></returns>
        public static int GetAsInt()
        {
            return (int)_activeInstrumentationType;
        }

        // PUT api/instrumentation/5
        /// <summary>
        /// Put
        /// </summary>
        /// <param name="instrumentationType"></param>
        public static void Put(InstrumentationType instrumentationType)
        {
            _activeInstrumentationType = instrumentationType;
        }

    }
}