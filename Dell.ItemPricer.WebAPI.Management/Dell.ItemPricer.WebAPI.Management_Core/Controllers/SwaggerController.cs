﻿using System;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    [RoutePrefix("swagger")]
    [ItemPricerLogExceptionFilter]
    public class SwaggerController : ControllerBase
    {
        [Route("schema")]
        public object GetSwaggerSchema()
        {
            var path = string.Format(@"{0}\App_Data\swagger.json", AppDomain.CurrentDomain.BaseDirectory);
            var swagger = System.IO.File.ReadAllText(path);
            var result = JsonConvert.DeserializeObject(swagger);
            return result;
        }
    }
}
