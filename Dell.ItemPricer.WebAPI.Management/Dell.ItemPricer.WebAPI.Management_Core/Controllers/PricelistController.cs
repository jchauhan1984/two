﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Description;
using Dell.Commerce.Services.Core.Aspects;
using Dell.Commerce.Services.LoggingProviders;
using Dell.ItemPricer.Data.DAO;
using Dell.ItemPricer.Documents.StatusReturn;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using PricingContract.Common;
using Swashbuckle.Swagger.Annotations;
using PriceList = Dell.ItemPricer.Documents.PriceList.PriceList;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    /// <summary>
    /// This controller handles all operations related to Price Lists.
    /// </summary>
    [RoutePrefix("{sellerName}")]
    [ItemPricerLogExceptionFilter]
    public class PricelistController : ControllerBase
    {
        private IDataAccess _dataAccess;

        public PricelistController()
        {
        }

        /// <summary>
        /// Controller constructor used for unit testing.
        /// </summary>
        /// <param name="dataAccess">IDataAccess object to use</param>
        public PricelistController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        /// <summary>
        /// Inserts a Price List and associated Price Breaks into the database. All necessary fields for a
        /// Price List and associated Price Breaks must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="pricelist">JSON representation of a complete Price List</param>
        /// <returns>JSON Response for Price List operation and HTTP Code</returns>
        [Route("insert/pricelist")]
        [SwaggerResponse(HttpStatusCode.Created, "Price List created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Price List already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceList))]
        [HttpPost]
        public HttpResponseMessage PostPricelist(string sellerName, [FromBody] PriceList pricelist)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertPriceList(pricelist);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts multiple Price Lists and associated Price Breaks into the database. The JSON representation
        /// would be an array of Price List. All necessary fields for a Price List and associated Price Breaks
        /// must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="pricelists">JSON representation of an array of complete Price Lists</param>
        /// <returns>JSON Response for each Price List operation and HTTP Code</returns>
        [Route("insert/pricelists")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Lists created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Price List already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceList>))]
        [HttpPost]
        public HttpResponseMessage PostPricelists(string sellerName, [FromBody] IEnumerable<PriceList> pricelists)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertPriceLists(pricelists);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Updates an individual Price List and associated Price Breaks. The Price List and Price Breaks
        /// need only include required fields and those that will be updated. Fields not changing need not
        /// be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="pricelist">JSON representation of the Price List to update</param>
        /// <returns>JSON Response for Price List operation and HTTP Code</returns>
        [Route("update/pricelist")]
        [SwaggerResponse(HttpStatusCode.Accepted, "Price List updated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\nPrice List to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Price List is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceList))]
        [HttpPut]
        public HttpResponseMessage PutPricelist(string sellerName, [FromBody] PriceList pricelist)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var updateReturn = dao.UpdatePriceList(pricelist);
                return BuildHttpResponse(updateReturn.HttpCode, updateReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts or Updates a set of Price Lists and associated Price Breaks. This method will determine automatically
        /// whether to Insert a Price List (seller pricelist ID not found) or to Update a Price List (seller pricelist ID
        /// found). The rules for Insert and Update are the same here as they are for the specific Insert and Update methods.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="pricelists">JSON representation of an array of complete Price Lists</param>
        /// <returns>JSON Response for each Price List operation and HTTP Code</returns>
        [Route("upsert/pricelists")]
        [SwaggerResponse(HttpStatusCode.Created, "Price List Upserted successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\\Update\nPrice List to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Price List is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceList>))]
        [HttpPost]
        public HttpResponseMessage UpsertPricelist(string sellerName, [FromBody] IEnumerable<PriceList> pricelists)
        {
            var context = new RequestContext()
            {
                RegistrationId = Guid.NewGuid(),
                RequestId = Guid.NewGuid().ToString()
            }; 
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var upsertReturn = dao.UpsertPriceLists(pricelists);
                return BuildHttpResponse(upsertReturn.HttpCode, upsertReturn.OperationStatuses);
            }
        } 

        /// <summary>
        /// Deletes a Price List from the database. Does not delete any associated Price Breaks. The Delete
        /// only sets the Price List to inactive and sets the Expiration Date to today. To reactivate, use
        /// Update and reset the IsActive flag and the Expiration Date.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="sellerPriceListId">Seller Price List ID of the Price List to delete</param>
        /// <returns>JSON Response for Price List operation and HTTP Code</returns>
        [Route("delete/pricelist")]
        [SwaggerResponse(HttpStatusCode.OK, "Price List deactivated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Price List to Delete was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Price List is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceList))]
        [HttpDelete]
        public HttpResponseMessage DeletePricelist(string sellerName, string sellerPriceListId)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var deleteReturn = dao.DeletePriceList(sellerPriceListId);
                return BuildHttpResponse(deleteReturn.HttpCode, deleteReturn.OperationStatuses);
            }
        }

        private static HttpResponseMessage BuildHttpResponse(string title, string message, HttpStatusCode statusCode)
        {
            return new HttpResponseMessage(statusCode)
            {
                ReasonPhrase = title,
                Content = new StringContent(message)
            };
        }

        private static HttpResponseMessage BuildHttpResponse(HttpStatusCode httpCode, List<OperationStatus> statuses)
        {
            var serStatus = JsonConvert.SerializeObject(statuses, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return new HttpResponseMessage(httpCode)
            {
                Content = new StringContent(serStatus, Encoding.UTF8, "application/json")
            };
        }
    }
}
