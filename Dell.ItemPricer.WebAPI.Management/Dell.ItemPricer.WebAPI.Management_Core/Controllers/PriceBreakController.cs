﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http.Description;
using Dell.ItemPricer.Data.DAO;
using Dell.ItemPricer.Documents.StatusReturn;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Swashbuckle.Swagger.Annotations;
using PriceBreak = Dell.ItemPricer.Documents.PriceBreak;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    /// <summary>
    /// This controller handles all operations related to Price Breaks.
    /// </summary>
    [RoutePrefix("{sellerName}")]
    [ItemPricerLogExceptionFilter]
    public class PriceBreakController : ControllerBase
    {
        private IDataAccess _dataAccess;

        public PriceBreakController()
        {
        }

        /// <summary>
        /// Controller constructor used for unit testing.
        /// </summary>
        /// <param name="dataAccess">IDataAccess object to use</param>
        public PriceBreakController(IDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
        }

        /// <summary>
        /// Inserts a Price Break in to the database. All necessary fields for an Price Break must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreak">JSON representation of a complete Price Break</param>
        /// <returns>JSON Response for Price Break operation and HTTP Code</returns>
        [Route("insert/pricebreak")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Break created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Price Break already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceBreak))]
        [HttpPost]
        public HttpResponseMessage PostPriceBreak(string sellerName, [FromBody] PriceBreak.PriceBreak priceBreak)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertPriceBreak(priceBreak);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts multiple Price Breaks into the database. The JSON represenation would be an array of Price Break.
        /// All necessary fields for a Price Breaks must be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreaks">JSON representation of an array of complete Price Breaks</param>
        /// <returns>JSON Response for each Price Break operation and HTTP Code</returns>
        [Route("insert/pricebreaks")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Breaks created successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert")]
        [SwaggerResponse(HttpStatusCode.Conflict, "Price Break already exists")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceBreak>))]
        [HttpPost]
        public HttpResponseMessage PostPriceBreaks(string sellerName, [FromBody] IEnumerable<PriceBreak.PriceBreak> priceBreaks)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var insertReturn = dao.InsertPriceBreaks(priceBreaks);
                return BuildHttpResponse(insertReturn.HttpCode, insertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Updates an individual Price Break. The Price Break need only include required fields and those
        /// that will be updated. Fields not changing need not be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreak">JSON representation of the Price Break to update</param>
        /// <returns>JSON Response for Price Break operation and HTTP Code</returns>
        [Route("update/pricebreak")]
        [SwaggerResponse(HttpStatusCode.Accepted, "Price Break updated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\nPrice Break to Update was not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceBreak))]
        [HttpPut]
        public HttpResponseMessage PutPriceBreak(string sellerName, [FromBody] PriceBreak.PriceBreak priceBreak)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var updateReturn = dao.UpdatePriceBreak(priceBreak);
                return BuildHttpResponse(updateReturn.HttpCode, updateReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Updates multiple Price Breaks in the database. The JSON representation would be an array or Price Break.
        /// Each individual Price Break need only include required fields and those that would be updated. Fields not
        /// changing need not be included.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreaks">JSON representation of an array of complete Price Breaks</param>
        /// <returns>JSON Response for Price Break operation and HTTP Code</returns>
        [Route("update/pricebreaks")]
        [SwaggerResponse(HttpStatusCode.Accepted, "Price Break updated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\nPrice Break to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Associated Price List or Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceBreak>))]
        [HttpPut]
        public HttpResponseMessage PutPriceBreaks(string sellerName, [FromBody] IEnumerable<PriceBreak.PriceBreak> priceBreaks)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var updateReturn = dao.UpdatePriceBreaks(priceBreaks);
                return BuildHttpResponse(updateReturn.HttpCode, updateReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Inserts or Updates a set of Price Breaks. This method will determine automatically whether to Insert a
        /// Price Break (seller price break ID not found) or to Update a Price Break (seller price break ID found).
        /// The rules for Insert and Update are the same here as they are for the specific Insert and Update methods.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreaks">JSON representation of an array of complete Price Breaks</param>
        /// <returns>JSON Response for each Price Break operation and HTTP Code</returns>
        [Route("upsert/pricebreaks")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Break Upserted successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Insert\\Update\nPrice List to Update was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Associated Price List or Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceBreak>))]
        [HttpPost]
        public HttpResponseMessage UpsertPriceBreaks(string sellerName, [FromBody] IEnumerable<PriceBreak.PriceBreak> priceBreaks)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var upsertReturn = dao.UpsertPriceBreaks(priceBreaks);
                return BuildHttpResponse(upsertReturn.HttpCode, upsertReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Links multiple Price Breaks to multiple Items or Price Lists. This method takes in a JSON object that
        /// contains a Seller Price Break ID and either a Seller Item ID or Seller Price List ID depending on what
        /// object needs to link to the Price Break. An array of these object can be passed to link multiple objects.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="priceBreakRequests">JSON representation of a Seller Price Break ID and other Seller object ID</param>
        /// <returns>JSON Response for each Price Break operation and HTTP Code</returns>
        [Route("link/pricebreaks")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Break Linked successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Link\nPrice Break to Link was not found")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Price List, Item, or Price Break was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Associated Price List or Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(IEnumerable<Documents.StatusReturn.PriceBreak>))]
        [HttpPut]
        public HttpResponseMessage LinkPriceBreaks(string sellerName, [FromBody] IEnumerable<PriceBreak.PriceBreakRequest> priceBreakRequests)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var linkReturn = dao.LinkPriceBreaks(priceBreakRequests);
                return BuildHttpResponse(linkReturn.HttpCode, linkReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Links a specific Price Break to a specific Price List in the database. This method can be used to attach
        /// certain price breaks to certain price lists by passing the Seller Price Break ID of the price break to link
        /// and the Seller Price List ID of the price list to link to.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="sellerPriceBreakId">Seller Price Break ID of the Price Break to link</param>
        /// <param name="sellerPriceListId">Seller Price List ID of the Price List to link to</param>
        /// <param name="applySequence">The sequence in which to apply this Price Break</param>
        /// <returns>JSON Response for each Price Break operation and HTTP Code</returns>
        [Route("link/pricebreak/{sellerPriceBreakId:minlength(3)}/pricelist/{sellerPriceListId:minlength(3)}")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Break Linked successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Link\nPrice Break to Link was not found")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Price List or Price Break was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Associated Price List is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceBreak))]
        [HttpPut]
        public HttpResponseMessage LinkPriceBreakToPricelist(string sellerName, string sellerPriceBreakId, string sellerPriceListId, int applySequence = 1)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var linkReturn = dao.LinkPriceListPriceBreak(sellerPriceListId, sellerPriceBreakId, applySequence);
                return BuildHttpResponse(linkReturn.HttpCode, linkReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Links a specific Price Break to a specific Item in the database. This method can be used to attach
        /// certain price breaks to certain items by passing the Seller Price Break ID of the price break to link
        /// and the Seller Item ID of the price list to link to.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="sellerPriceBreakId">Seller Price Break ID of the Price Break to link</param>
        /// <param name="sellerItemId">Seller Item ID of the Item to link to</param>
        /// <param name="sellerPriceListId">Seller Price List ID of the Price List that Item belongs to</param>
        /// <param name="applySequence">The sequence in which to apply this Price Break</param>
        /// <returns>JSON Response for each Price Break operation and HTTP Code</returns>
        [Route("link/pricebreak/{sellerPriceBreakId:minlength(3)}/item/{sellerItemId:minlength(3)}/pricelist/{sellerPriceListId:minlength(3)}")]
        [SwaggerResponse(HttpStatusCode.Created, "Price Break Linked successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Nothing was passed in to Link\nPrice Break to Link was not found")]
        [SwaggerResponse(HttpStatusCode.NotFound, "Price List, Item, or Price Break was not found")]
        [SwaggerResponse(HttpStatusCode.ServiceUnavailable, "Associated Item is currently being processed by another service")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceBreak))]
        [HttpPut]
        public HttpResponseMessage LinkPriceBreakToItem(string sellerName, string sellerPriceBreakId, string sellerItemId, string sellerPriceListId, int applySequence = 1)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var linkReturn = dao.LinkItemPriceBreak(sellerItemId, sellerPriceBreakId, sellerPriceListId, applySequence);
                return BuildHttpResponse(linkReturn.HttpCode, linkReturn.OperationStatuses);
            }
        }

        /// <summary>
        /// Deletes a Price Break from the database. The Delete only sets the Price Break to inactive and sets the
        /// Expiration Date to today. To reactivate, use Update and reset the IsActive flag and the Expiration Date.
        /// </summary>
        /// <param name="sellerName">Name of the Seller database to update</param>
        /// <param name="sellerPriceBreakId">Seller Price Break ID of the Price Break to delete</param>
        /// <returns>JSON Response for Price Break operation and HTTP Code</returns>
        [Route("delete/pricebreak")]
        [SwaggerResponse(HttpStatusCode.OK, "Price Break deactivated successfully")]
        [SwaggerResponse(422, "There was a validation error")]
        [SwaggerResponse(HttpStatusCode.BadRequest, "Price Break to Delete was not found")]
        [SwaggerResponse(HttpStatusCode.InternalServerError, "There was a database error")]
        [ResponseType(typeof(Documents.StatusReturn.PriceBreak))]
        [HttpDelete]
        public HttpResponseMessage DeletePriceBreak(string sellerName, string sellerPriceBreakId)
        {
            if (_dataAccess == null) _dataAccess = new DataAccess(sellerName);

            using (var dao = _dataAccess)
            {
                if (!dao.DataConnection.Success)
                {
                    return BuildHttpResponse(dao.DataConnection.Message,
                        dao.DataConnection.MoreInfo,
                        HttpStatusCode.InternalServerError);
                }

                var deleteReturn = dao.DeletePriceBreak(sellerPriceBreakId);
                return BuildHttpResponse(deleteReturn.HttpCode, deleteReturn.OperationStatuses);
            }
        }

        private static HttpResponseMessage BuildHttpResponse(string title, string message, HttpStatusCode statusCode)
        {
            return new HttpResponseMessage(statusCode)
            {
                ReasonPhrase = title,
                Content = new StringContent(message)
            };
        }

        private static HttpResponseMessage BuildHttpResponse(HttpStatusCode httpCode, List<OperationStatus> statuses)
        {
            var serStatus = JsonConvert.SerializeObject(statuses, Formatting.Indented, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            return new HttpResponseMessage(httpCode)
            {
                Content = new StringContent(serStatus, Encoding.UTF8, "application/json")
            };
        }
    }
}
