﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Net;
using System.Net.Http;
using System.Web;

namespace Dell.ItemPricer.WebAPI.Management_Core.Controllers
{
    public class IndexController : ControllerBase
    {
        [AllowAnonymous]
        [Route("")]
        [ItemPricerLogExceptionFilter]
        public HttpResponseMessage GetIndex()
        {
            var response = Request.CreateResponse(HttpStatusCode.Moved);
            string fullyQualifiedUrl = Request.RequestUri.GetLeftPart(UriPartial.Authority) + VirtualPathUtility.ToAbsolute("~/").TrimEnd('/');
            response.Headers.Location = new Uri(fullyQualifiedUrl + "/swagger/ui/index");
            return response;
        }
    }
}
