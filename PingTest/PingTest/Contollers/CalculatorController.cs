﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace PingTest.Contollers
{

    public class CalculatorController : ControllerBase
    {
        /// <summary>
        /// This is for addition of numbers
        /// </summary>
        /// <param name="a"> Mandatory </param>
        /// <param name="b"> Mandatory </param>
        /// <returns></returns>
        [Route("api/Add")]
        [HttpPost]
        public string Add(int a,int b)
        {
            int c = a + b;
            return c.ToString();
        }

        [Route("api/Sub")]
        [HttpPost]
        public string Sub(int a, int b)
        {
            int c = a - b;
            return c.ToString();
        }

        [Route("api/Div")]
        [HttpPost]
        public string Div(int a, int b)
        {
            int c = a / b;
            return c.ToString();
        }

        [Route("api/Mul")]
        [HttpPost]
        public string Mul(int a, int b)
        {
            int c = a * b;
            return c.ToString();
        }

    }
}