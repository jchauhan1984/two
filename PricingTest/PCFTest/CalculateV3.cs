﻿using System;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace PCFTest
{
    [TestClass]
    public class CalculateV3
    {
        public IWebDriver driver;

        [TestInitialize]
        public void TestInitialize()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Manage().Cookies.DeleteAllCookies();
            driver.Url = "https://test-service.ausvdc02.pcf.dell.com/swagger/ui/index";
            WebElements.txtURL(driver).Clear();
            WebElements.txtURL(driver).SendKeys("https://test-service.ausvdc02.pcf.dell.com:/swagger/schema");
            WebElements.btnExplore(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
        }
        [TestMethod]
        public void V3CalculateAllPrices()
        {
                WebElements.tabCalculateAllPrices(driver).Click();
                ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", WebElements.txtCalculateAllPriceRequest(driver),TextRead.readFile());
                WebElements.btnTryCalculateAllPriceRequest(driver).Click();
                driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
                String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", WebElements.responseCodeforCalculateAllPrice(driver));
                Assert.AreEqual(200,actualcode);

        }

        [TestMethod]
        public void CalculateCartSummaryWithCustomer()
        {
            WebElements.tabV3CalculateCartSummaryWithCustomer(driver).Click();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", WebElements.txtV3CalculateCartSummaryWithCustomerRequest(driver), TextRead.readFile());
            WebElements.btnTryCalculateCartSummaryWithCustomer(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", WebElements.responseCodeforV3CalculateCartSummaryWithCustomer(driver));
            Assert.AreEqual(200,actualcode);
        }

        ///V3/Calculate/Cart/Summary/WithoutCustomer

        [TestMethod]
        public void CalculateCartSummaryWithoutCustomer()
        {
            WebElements.tabV3CalculateCartSummaryWithoutCustomer(driver).Click();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", WebElements.txtV3CalculateCartSummaryWithoutCustomerRequest(driver), TextRead.readFile());
            WebElements.btnTryCalculateCartSummaryWithoutCustomer(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", WebElements.responseCodeCalculateCartSummaryWithoutCustomer(driver));
            Assert.AreEqual(200,actualcode);
        }

        [TestMethod]
        public void CalculateCartDetailedWithCustomer()
        {
            WebElements.tabV3CalculateCartDetailedWithCustomer(driver).Click();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", WebElements.txtV3CalculateCartDetailedWithCustomerRequest(driver), TextRead.readFile());
            WebElements.btnTryV3CalculateCartDetailedWithCustomer(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", WebElements.responseCodeCalculateCartDetailedWithCustomer(driver));
            Assert.AreEqual(200, actualcode);
        }

        [TestMethod]
        public void CalculateCartDetailedWithoutCustomer()
        {
            WebElements.tabV3CalculateCartDetailedWithoutCustomer(driver).Click();
            ((IJavaScriptExecutor)driver).ExecuteScript("arguments[0].value = arguments[1];", WebElements.txtV3CalculateCartDetailedWithoutCustomerRequest(driver), TextRead.readFile());
            WebElements.btnTryV3CalculateCartDetailedWithoutCustomer(driver).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            String actualcode = (string)((IJavaScriptExecutor)driver).ExecuteScript("return $(arguments[0]).text();", WebElements.responseCodeV3CalculateCartDetailedWithoutCustomer(driver));
            Assert.AreEqual(200,actualcode);
        }

        [TestCleanup]
        public void TestCleanUp()
        {
           driver.Close();
        }
        

    }
    }
