﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PCFTest
{
    class WebElements
    {
        public static IWebElement element;

        //Webelements for Home Page

        //Text: For providing the URL
        public static IWebElement txtURL(IWebDriver driver)
        {
            element = driver.FindElement(By.Id("input_baseUrl"));
            return element;
        }

        //Button: Explore
        public static IWebElement btnExplore(IWebDriver driver)
        {
            element = driver.FindElement(By.Id("explore"));
            return element;
        }

        //++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

        //Header: Calculate **********

        //Tab: CalculateShippingCharge
        public static IWebElement tabCalShippingCharge(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Calculate_Calculate_ShippingChargeWithDiscount']/div[1]/h3/span[1]/a"));
            return element;
        }

        //Text: Request
        public static IWebElement txtServiceCalShippingCharge(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Calculate_Calculate_ShippingChargeWithDiscount_content']/form/table[1]/tbody/tr/td[2]/textarea"));

            return element;
        }

        //Button: Try
        public static IWebElement btnTryCalShippingCharge(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='Calculate_Calculate_ShippingChargeWithDiscount_content']/form/div[3]/input"));
            return element;
        }

        //+++++++++++++++++++++++++++++++++++++++++++++++
        //Header: CalculateItem

        //Tab: CalculateItem/ServiceEntitlement
        public static IWebElement tabCalItemSerEnt(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/CalculateItem/ServiceEntitlement']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtServiceEntitlement(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateItem_CalculateItem_ServiceEntitlement_content']/form/table[1]/tbody/tr[1]/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryCalItemSerEnt(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateItem_CalculateItem_ServiceEntitlement_content']/form/div[3]/input"));
            return element;
        }

        //Tab: CalculateItem/ListPriceDiscounts/SoftwareAndAccessories
        public static IWebElement tabCalItemListPriceDiscount(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/CalculateItem/ListPriceDiscounts/SoftwareAndAccessories']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtCallItemListPriceDiscountRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateItem_CalculateItem_SoftwareAndAccessories_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryCallItemListPriceDiscount(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateItem_CalculateItem_SoftwareAndAccessories_content']/form/div[3]/input"));
            return element;
        }

        //+++++++++++++++++++++++++++++++++++++++
        //Header: CalculateV3

        //Tab: /V3/Calculate/allprices
        public static IWebElement tabCalculateAllPrices(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/V3/Calculate/allprices']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtCalculateAllPriceRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_AllPrices_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryCalculateAllPriceRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_AllPrices_content']/form/div[3]/input"));
            return element;
        }

        //Field: Response
        public static IWebElement responseCodeforCalculateAllPrice(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//div[@id='CalculateV3_CalculateV3_AllPrices_content']/div/div[@class='block response_code']/pre"));
            return element;
        }

        //Tab: /V3/Calculate/Cart/Summary/WithCustomer
        public static IWebElement tabV3CalculateCartSummaryWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/V3/Calculate/Cart/Summary/WithCustomer']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtV3CalculateCartSummaryWithCustomerRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithCustomerSummaryCartView_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryCalculateCartSummaryWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithCustomerSummaryCartView_content']/form/div[3]/input"));
            return element;
        }

        //Field: Response
        public static IWebElement responseCodeforV3CalculateCartSummaryWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithCustomerSummaryCartView_content']/div[2]/div[4]/pre"));
            return element;
        }

        //Tab: /V3/Calculate/Cart/Summary/WithoutCustomer
        public static IWebElement tabV3CalculateCartSummaryWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/V3/Calculate/Cart/Summary/WithoutCustomer']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtV3CalculateCartSummaryWithoutCustomerRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryCalculateCartSummaryWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/form/div[3]/input"));
            return element;
        }

        //Field:Response
        public static IWebElement responseCodeCalculateCartSummaryWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/div[2]/div[4]/pre"));
            return element;
        }

        
        //Tab: /V3/Calculate/Cart/Detailed/WithCustomer
        public static IWebElement tabV3CalculateCartDetailedWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/V3/Calculate/Cart/Summary/WithoutCustomer']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtV3CalculateCartDetailedWithCustomerRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryV3CalculateCartDetailedWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/form/div[3]/input"));
            return element;
        }

        //Field:Response
        public static IWebElement responseCodeCalculateCartDetailedWithCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerSummaryCartView_content']/div[2]/div[4]/pre"));
            return element;
        }

        //Tab: /V3/Calculate/Cart/Detailed/WithoutCustomer
        public static IWebElement tabV3CalculateCartDetailedWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//a[text()='/V3/Calculate/Cart/Detailed/WithoutCustomer']"));
            return element;
        }

        //Text: Request
        public static IWebElement txtV3CalculateCartDetailedWithoutCustomerRequest(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerDetailedCartView_content']/form/table[1]/tbody/tr/td[2]/textarea"));
            return element;
        }

        //Button: Try
        public static IWebElement btnTryV3CalculateCartDetailedWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerDetailedCartView_content']/form/div[3]/input"));
            return element;
        }

        //Field: Response
        public static IWebElement responseCodeV3CalculateCartDetailedWithoutCustomer(IWebDriver driver)
        {
            element = driver.FindElement(By.XPath("//*[@id='CalculateV3_CalculateV3_WithoutCustomerDetailedCartView_content']/div[2]/div[4]/pre"));
            return element;
        }



    }
}
